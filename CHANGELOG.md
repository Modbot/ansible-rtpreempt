# Ansible RTPreempt Kernel Patch
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [1.0.1] - 2018-07-26

### Fixed

- Kernel source no longer deleted after kernel installation. Only source archives are deleted.


## [1.0.0] - 2018-03-30

### Added

- Add tasks used to patch and install kernel.